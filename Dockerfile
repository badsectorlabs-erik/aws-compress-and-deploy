FROM node:9.4.0
MAINTAINER Erik <erik@badsectorlabs.com>

RUN echo "deb http://ftp.de.debian.org/debian testing main" >> /etc/apt/sources.list && \
    apt-get update && \
    apt-get install -y -t testing python3.6 && \
    apt-get clean

RUN npm install -g browserify && \
    npm install -g uglify-js && \
    npm install -g html-minify && \
    npm install -g uglifycss

RUN apt-get update && \
    apt-get install -y zip && \
    apt-get install -y python-pip && \
    apt-get install -y python-software-properties && \
    apt-get install -y jq && \
    apt-get install -y groff && \
    apt-get install -y git && \
    apt-get install -y software-properties-common && \
    apt-get clean

RUN pip install --upgrade pip && \
    pip install awscli && \
    pip install virtualenv

CMD ["aws"]