# aws-compress-and-deploy

Based on: https://github.com/salte-io/aws-lambda-deploy

Docker Hub: https://hub.docker.com/r/badsectorlabs/aws-compress-and-deploy/

## Description

Creates an environment containing both the AWS CLI and HTML/CSS/JS minifiers.  It can be used to deploy AWS Lambda functions or static sites to S3.
## Instructions

There are two primary use cases that this image was created to support.
1. For use within a Gitlab CI YAML file.
2. For use by an interactive user.

The following are simple examples of each use case.

### _.gitlab-ci.yml File Python3.6 Lambda Deploy Example_

```yml
image: badsectorlabs/aws-compress-and-deploy:latest

variables:
  # Keys and secrets are defined in the project CI settings and exposed as env variables
  AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
  AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
  AWS_DEFAULT_REGION: "us-east-1"
  NAME: "MyFunction" # Lambda function name
  FILENAME: "MyFunction.py"
  RUNTIME: "python3.6"
  ROLE: "arn:aws:iam::000000000000:role/xxxxx"
  HANDLER: "MyFunction.lambda_handler"
  FILE: "fileb://deploy_$CI_COMMIT_REF_NAME.zip"
 
stages:
 - deploy

deploy-staging:
  stage: deploy
  variables:
    ALIAS: "Staging"
    DESC: "Staging build, commit: $CI_COMMIT_SHA"
  script:
    - virtualenv -p /usr/bin/python3.6 env
    - source env/bin/activate
    - pip install -r requirements.txt
    - mkdir dist
    - cp $FILENAME dist # copy all files needed to dist
    # Copy any other directories (modules, etc) here
    - cp -rf env/lib/python3.6/site-packages/* dist
    - cd dist
    - zip -r9 ../deploy_$CI_COMMIT_REF_NAME.zip .
    - cd ..
    - deactivate
    - ls -lart
    - echo Creating or updating $NAME
    - > # This captures the code hash for the updated/created lambda function; -r is needed with jq to strip the quotes
      CODE_SHA_256=$(aws lambda update-function-code --function-name $NAME --zip-file $FILE | jq -r ."CodeSha256" || aws lambda create-function
      --function-name $NAME --runtime $RUNTIME --role $ROLE --handler $HANDLER --zip-file $FILE | jq -r ."CodeSha256")
    - echo Publishing LATEST, CodeSha256=$CODE_SHA_256, as 'Staging'
    - VERSION=$(aws lambda publish-version --function-name $NAME --description "$DESC" --code-sha-256 $CODE_SHA_256 | jq -r ."Version")
    - echo "Published LATEST as version $VERSION"
    - >
      aws lambda update-alias --function-name $NAME --name $ALIAS --function-version $VERSION || aws lambda create-alias
      --function-name $NAME --name $ALIAS --description "Staging" --function-version $VERSION
    - echo Successfully updated $NAME:$ALIAS
  environment:
    name: master-staging
  only:
    - master

deploy-prod:
  stage: deploy
  variables:
    ALIAS: "Prod"
    DESC: "Prod build, commit: $CI_COMMIT_SHA"
  script:
    - virtualenv -p /usr/bin/python3.6 env
    - source env/bin/activate
    - pip install -r requirements.txt
    - mkdir dist
    - cp $FILENAME dist # copy all files needed to dist
    # Copy any other directories (modules, etc) here
    - cp -rf env/lib/python3.6/site-packages/* dist
    - cd dist
    - touch PROD # This is the canary that will tell the lambda function to use the PROD secrets
    - zip -r9 ../deploy_$CI_COMMIT_REF_NAME.zip .
    - cd ..
    - deactivate
    - ls -lart
    - echo Creating or updating $NAME
    - > # This captures the code hash for the updated/created lambda function; -r is needed with jq to strip the quotes
      CODE_SHA_256=$(aws lambda update-function-code --function-name $NAME --zip-file $FILE | jq -r ."CodeSha256" || aws lambda create-function
      --function-name $NAME --runtime $RUNTIME --role $ROLE --handler $HANDLER --zip-file $FILE | jq -r ."CodeSha256")
    - echo Publishing LATEST, CodeSha256=$CODE_SHA_256, as 'Prod'
    - VERSION=$(aws lambda publish-version --function-name $NAME --description "$DESC" --code-sha-256 $CODE_SHA_256 | jq -r ."Version")
    - echo "Published LATEST as version $VERSION"
    - >
      aws lambda update-alias --function-name $NAME --name $ALIAS --function-version $VERSION || aws lambda create-alias
      --function-name $NAME --name $ALIAS --description "Prod" --function-version $VERSION
    - echo Successfully updated $NAME:$ALIAS
  environment:
    name: master-prod
  only:
    - master
  when: manual
```

### _.gitlab-ci.yml File Static Site S3 Deploy Example_

```yml
variables:
  # Keys and secrets are defined in the project CI settings and exposed as env variables
  AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
  AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
  AWS_DEFAULT_REGION: "us-east-1"

stages:
 - deploy
 
deploy-staging:
  stage: deploy
  image: badsectorlabs/aws-compress-and-deploy:latest
  variables:
    DESC: "Staging build, commit: $CI_COMMIT_SHA"
    S3_BUCKET: some.staging.bucket.name
  script:
    - cd output # Assumes the static site is in 'output'
    - echo [+] ls before minification
    - ls -lart .
    - echo "$DESC" > version.html
    - echo [+] minifying HTML
    - find . -iname \*.html | xargs -I {} htmlminify -o {} {}
    - echo [+] minifying CSS
    - find . -iname \*.css | xargs -I {} uglifycss --output {} {}
    - echo [+] minifying JS
    - find . -iname \*.js | xargs -I {} uglifyjs -o {} {}
    - echo [+] ls after minification
    - ls -lart .
    - > # only push changes to staging unless "force sync" is part of the last commit message - saves S3 puts
      if [[ "$(git log -1 --pretty=%B)" == *"force sync"* ]]; then
        echo [+] Syncing all files to $S3_BUCKET
        aws s3 sync . s3://$S3_BUCKET
      elif git log -m -1 --name-only --pretty="format:" | grep output >/dev/null; then
        echo [+] Pushing to S3
        git log -m -1 --name-only --pretty="format:" | grep output | sed 's#output/##g' | xargs -I {} aws s3 cp {} s3://$S3_BUCKET/{}
        aws s3 cp version.html s3://$S3_BUCKET/version.html
      else
        echo [+] Nothing to push to S3
      fi
  environment:
    name: master-staging
  only:
    - master

deploy-prod:
  stage: deploy
  image: badsectorlabs/aws-compress-and-deploy:latest
  variables:
    DESC: "Prod build, commit: $CI_COMMIT_SHA"
    S3_BUCKET: some.bucket.name
    CLOUDFRONT_DISTRIBUTION_ID: XXXXXXXXXXXXXX
  script:
    - cd output # Assumes the static site is in 'output'
    - echo [+] ls before minification
    - ls -lart .
    - echo "$DESC" > version.html
    - echo [+] minifying HTML
    - find . -iname \*.html | xargs -I {} htmlminify -o {} {}
    - echo [+] minifying CSS
    - find . -iname \*.css | xargs -I {} uglifycss --output {} {}
    - echo [+] minifying JS
    - find . -iname \*.js | xargs -I {} uglifyjs -o {} {}
    - echo [+] ls after minification
    - ls -lart .
    - echo [+] Syncing all files to $S3_BUCKET
    - aws s3 sync . s3://$S3_BUCKET # Always sync prod because there may have been many pushes to staging between prod pushes
    - echo [+] Invalidating Cloudfront cache
    - aws cloudfront create-invalidation --distribution-id $CLOUDFRONT_DISTRIBUTION_ID --paths '/*'
  environment:
    name: master-prod
  only:
    - master
  when: manual
```

### _AWS Interactive Example_

```sh
$ docker run -it badsectorlabs/aws-compress-and-deploy sh
/ # aws help
NAME
  aws -

DESCRIPTION
  The AWS Command Line Interface is a unified tool to manage your AWS
  services.

SYNOPSIS
  aws [options] <command> <subcommand> [parameters]

  Use aws command help for information on a specific command.  Use aws
  help topics to view a list of available help topics. The synopsis for
  each command shows its parameters and their usage. Optional parameters
  are shown in square brackets.
...
```